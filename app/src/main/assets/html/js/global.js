function invokeNative(useCase, payload) {
    showNativeMessage("Use case invoked:\n" + useCase + "(\"" + payload + "\")");
    window[useCase].postMessage(payload);
}

function showHTMLMessage(data) {
    var text = JSON.stringify(data, null, 4);
    document.getElementById('htmlMessages').textContent = text;
}

function showNativeMessage(method, data) {
    var text = (data !== undefined) ? method + "\n" + JSON.stringify(data, null, 4) : method;
    document.getElementById('nativeMessages').textContent = text;
}
