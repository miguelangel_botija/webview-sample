package com.bbva.next.webviewsample

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        configureWebView(webView)
        button.setOnClickListener { onClickButton() }
    }

    @SuppressLint("SetJavascriptEnabled", "JavascriptInterface")
    private fun configureWebView(webView: WebView) {
        webView.settings.javaScriptEnabled = true
        webView.addJavascriptInterface(UseCase(this), UseCase.NAME)
        webView.loadUrl("file:///android_asset/html/native-web.html")
    }

    @SuppressLint("SetTextI18n")
    private fun onClickButton() {
        val message = editText.text.toString()
        textView.text = "Event invoked:\nEventFunction(\"$message\")"
        injectJs("""EventFunction("$message")""")
    }

    private fun injectJs(callbackResponse: String) {
        webView?.evaluateJavascript(callbackResponse) {
            Log.d("", "onReceiveValue: $it")
        }
    }

    override fun sendMessage(message: String) {
        textView.text = message
    }
}
