package com.bbva.next.webviewsample

import java.lang.ref.WeakReference
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun <T> weak(initialValue: T? = null) = WeakDelegate(initialValue)

class WeakDelegate<T>(initialValue: T?) : ReadWriteProperty<Any?, T?> {

    var value = WeakReference(initialValue)

    override fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        return value.get()
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        this.value = WeakReference(value)
    }
}
