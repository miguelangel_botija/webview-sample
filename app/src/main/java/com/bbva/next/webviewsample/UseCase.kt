/*
 * Copyright (c) 2018 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package com.bbva.next.webviewsample

import android.webkit.JavascriptInterface

@Suppress("unused")
class UseCase(view: View) {

    private var view by weak(view)

    companion object {
        const val NAME = "Native.execute"
    }

    /**
     * Receive an invocation from WebView.
     * @param message The message from WebView.
     */
    @JavascriptInterface
    fun postMessage(message: String) {
        view?.sendMessage(message)
    }
}

interface View {
    fun sendMessage(message: String)
}
